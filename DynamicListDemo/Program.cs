﻿// See https://aka.ms/new-console-template for more information

using Lab1Var10;

public class DynamicListDemo
{
    static void DemoAdd()
    {
        Console.WriteLine("============== DEMO ADD: add 0,1,2 and foreach ==============");
        var list = new DynamicList<int>();
        list.Add(0);
        list.Add(1);
        list.Add(2);

        foreach (var value in list)
        {
            Console.WriteLine(value);
        }
    }

    static void DemoClear()
    {
        Console.WriteLine("================ DEMO CLEAR: add 0,1,2 and clear ================");
        var list = new DynamicList<int>();
        list.Add(0);
        list.Add(1);
        list.Add(2);
        
        Console.WriteLine($"Count before: {list.Count}");
        list.Clear();
        Console.WriteLine($"Count after: {list.Count}; IsEmpty: {list.IsEmpty}");
    }

    static void DemoContains()
    {
        Console.WriteLine("================ DEMO CONTAINS: add 0,1,2 and check if contains 0 and 4 ================");
        var list = new DynamicList<int>();
        list.Add(0);
        list.Add(1);
        list.Add(2);
        
        Console.WriteLine($"Contains 0: {list.Contains(0)}");
        Console.WriteLine($"Contains 4: {list.Contains(4)}");
    }

    static void DemoCopyTo()
    {
        Console.WriteLine("================ DEMO CopyTo: add 0,1,2 and iterate array copy ================");
        var list = new DynamicList<int>();
        list.Add(0);
        list.Add(1);
        list.Add(2);

        var array = new int[3];
        list.CopyTo(array, 0);

        foreach (var value in array)
        {
            Console.WriteLine(value);
        }
    }

    static void DemoRemove()
    {
        Console.WriteLine("================ DEMO REMOVE: add 0,1,2 , remove 1 and compare internals ================");
        var list = new DynamicList<int>();
        list.Add(0);
        list.Add(1);
        list.Add(2);

        Console.WriteLine("Before:");
        foreach (var value in list)
        {
            Console.WriteLine(value);
        }

        list.Remove(1);
        
        Console.WriteLine("After:");
        foreach (var value in list)
        {
            Console.WriteLine(value);
        }
    }

    static void DemoIndexOf()
    {
        Console.WriteLine("================ DEMO IndexOf: add 0,36,2 and get index of 36 ================");
        var list = new DynamicList<int>();
        list.Add(0);
        list.Add(36);
        list.Add(2);
        
        Console.WriteLine($"Index of 36 is: {list.IndexOf(36)}");
    }
    
    static void DemoInsert()
    {
        Console.WriteLine("================ DEMO INSERT: add 0,1,2 , insert 14 at 1 and compare internals ================");
        var list = new DynamicList<int>();
        list.Add(0);
        list.Add(1);
        list.Add(2);

        Console.WriteLine("Before:");
        foreach (var value in list)
        {
            Console.WriteLine(value);
        }

        list.Insert(1, 14);
        
        Console.WriteLine("After:");
        foreach (var value in list)
        {
            Console.WriteLine(value);
        }
    }

    static void DemoRemoveAt()
    {
        Console.WriteLine("================ DEMO RemoveAt: add 0,1,2 , remove at 1 and compare internals ================");
        var list = new DynamicList<int>();
        list.Add(0);
        list.Add(1);
        list.Add(2);

        Console.WriteLine("Before:");
        foreach (var value in list)
        {
            Console.WriteLine(value);
        }

        list.RemoveAt(1);
        
        Console.WriteLine("After:");
        foreach (var value in list)
        {
            Console.WriteLine(value);
        }
    }

    static void DemoSubscript()
    {
        Console.WriteLine("================ DEMO SUBSCRIPT: add 0,1,2 and use subscript getter and setter ================");
        var list = new DynamicList<int>();
        list.Add(0);
        list.Add(1);
        list.Add(2);
        
        Console.WriteLine($"Get [0]: {list[0]}");
        list[0] = 60;
        Console.WriteLine($"Set [0] = 60: {list[0]}");
    }

    static void DemoCreationEvent()
    {
        Console.WriteLine("================ DEMO CREATION EVENT: add 0,1,2 and subscribe ================");
        var list = new DynamicList<int>();
        list.ItemCreated += (_, args) => Console.WriteLine($"Item {args.Data} created at {args.Index}");
        list.Add(0);
        list.Add(1);
        list.Add(2);
    }

    static void DemoDeletoinEvent()
    {
        Console.WriteLine("================ DEMO DELETION EVENT: add 0,1,2 , delete them and subscribe ================");
        var list = new DynamicList<int>();
        list.ItemDeleted += (_, args) => Console.WriteLine($"Item {args.Data} deleted at {args.Index}");
        list.Add(0);
        list.Add(1);
        list.Add(2);

        while (!list.IsEmpty)
        {
            list.Remove(list.Count - 1);
        }
    }

    public static void Main()
    {
        DemoAdd();   
        DemoClear();
        DemoContains();
        DemoCopyTo();
        DemoRemove();
        DemoIndexOf();
        DemoInsert();
        DemoRemoveAt();
        DemoSubscript();
        DemoCreationEvent();
        DemoDeletoinEvent();
    }
    
}