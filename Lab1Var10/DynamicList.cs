﻿using System.Collections;

namespace Lab1Var10;

public class DynamicList<T>: IList<T>
{
    public int Count { get; private set; }
    
    public bool IsReadOnly => false;
    public bool IsEmpty => _head == null;
    
    public event EventHandler<ItemCreationEventArgs<T>>? ItemCreated;
    public event EventHandler<ItemDeletionEventArgs<T>>? ItemDeleted;

    private Node<T>? _head;
    private Node<T>? _tail;
    
    public DynamicList()
    {
        _head = _tail = null;
        Count = 0;
        ItemCreated = null;
        ItemDeleted = null;
    }

    public IEnumerator<T> GetEnumerator()
    {
        var currentNode = _head;
        if (currentNode is null) yield break;
        
        while (currentNode != null)
        {
            yield return currentNode.Data;
            currentNode = currentNode.Next;
        }
    }

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

    public void Add(T item)
    {
        var newNode = new Node<T>(item);
        if (_head is null)
        {
            _head = _tail = newNode;
        }
        else if (_tail is not null)
        {
            _tail.Next = newNode;
            _tail = newNode;
        }

        ItemCreated?.Invoke(this, new ItemCreationEventArgs<T>(item, Count));
        Count++;
    }

    public void Clear()
    {
        _head = _tail = null;
        Count = 0;
    }

    public bool Contains(T item)
    {
        if (IsEmpty) return false;
        if (item is null) throw new ArgumentNullException(nameof(item));
        
        var currentNode = _head;

        while (currentNode != null) 
        {
            if (item.Equals(currentNode.Data))
                return true;

            currentNode = currentNode.Next;
        }

        return false;
    }

    public void CopyTo(T[] array, int arrayIndex)
    {
        if (arrayIndex < 0 || arrayIndex > array.Length)
            throw new ArgumentOutOfRangeException(nameof(arrayIndex));

        var currentNode = _head;
        while (currentNode != null)
        {
            array[arrayIndex++] = currentNode.Data;
            currentNode = currentNode.Next;
        }
    }

    public bool Remove(T item)
    {
        if (IsEmpty) return false;
        if (item is null) throw new ArgumentNullException(nameof(item));

        var currentNode = _head!;
        Node<T>? parentNode = null;

        while (currentNode != null)
        {
            if (!item.Equals(currentNode.Data))
            {
                parentNode = currentNode;
                currentNode = currentNode.Next;
                continue;
            }

            if (parentNode is null)
            {
                _head = _tail = null;
            }
            else
            {
                parentNode.Next = currentNode.Next;
            }

            Count--;
            ItemDeleted?.Invoke(this, new ItemDeletionEventArgs<T>(currentNode.Data, Count));
            return true;
        }

        return false;
    }
    
    public int IndexOf(T item)
    {
        if (item is null) throw new ArgumentNullException(nameof(item));
        if (IsEmpty) return -1;

        var currentNode = _head;
        int index = 0;

        while (currentNode != null)
        {
            if (item.Equals(currentNode.Data))
                return index;

            currentNode = currentNode.Next;
            index++;
        }

        return -1;
    }

    public void Insert(int index, T item)
    {
        if (index > Count || index < 0)
        {
            throw new ArgumentOutOfRangeException(nameof(index));
        }
        
        var newNode = new Node<T>(item);
        
        Node<T>? currentNode = _head;
        Node<T>? parentNode = null;

        int currentIndex = 0;

        while (currentNode != null)
        {
            if (currentIndex == index)
            {
                if (parentNode is null)
                {
                    newNode.Next = _head;
                    _head = newNode;
                }
                else
                {
                    parentNode.Next = newNode;
                    newNode.Next = currentNode;
                }
                
                ItemCreated?.Invoke(this, new ItemCreationEventArgs<T>(item, Count));
                Count++;
                return;
            }

            currentIndex++;
            parentNode = currentNode;
            currentNode = currentNode.Next!;
        }

    }

    public void RemoveAt(int index)
    {
        if (index < 0 || index >= Count) throw new ArgumentOutOfRangeException(nameof(index));
        Node<T> currentNode = _head!;
        Node<T>? parentNode = null;
        int currentIndex = 0;

        while (currentNode != null)
        {
            if (currentIndex == index)
            {
                if (parentNode is null) 
                {
                    Clear();
                    return;
                }
                else
                {   
                    parentNode.Next = currentNode.Next;
                    Count--;
                    ItemDeleted?.Invoke(this, new ItemDeletionEventArgs<T>(currentNode.Data, Count));
                    return;
                }
            }
            
            currentIndex++;
            parentNode = currentNode;
            currentNode = currentNode.Next!;
        }
    }

    public T this[int index]
    {
        get => GetByIndex(index);
        set => Insert(index, value);
    }

    private T GetByIndex(int index)
    {
        if (index >= Count || index < 0) throw new ArgumentOutOfRangeException(nameof(index));

        var currentIndex = 0;
        var currentNode = _head;
        while (currentNode != null)
        {
            if (currentIndex == index)
                return currentNode.Data;

            currentNode = currentNode.Next;
            currentIndex++;
        }

        throw new KeyNotFoundException();
    }
}