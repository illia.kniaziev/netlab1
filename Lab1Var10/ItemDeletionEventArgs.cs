namespace  Lab1Var10;

public class ItemDeletionEventArgs<T> : EventArgs
{
    public T Data { get; }

    public int Index { get; }

    public ItemDeletionEventArgs(T data, int index)
    {
        Data = data;
        Index = index;
    }
}