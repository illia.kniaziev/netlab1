namespace  Lab1Var10;

public class ItemCreationEventArgs<T> : EventArgs
{
    public T Data { get; }

    public int Index { get; }

    public ItemCreationEventArgs(T data, int index)
    {
        Data = data;
        Index = index;
    }
}